#SR2_PROJET2_AgentFlopBox 
#Ferhat_HOCINE_M1GL


## 1- Intoduction :

L'objectif du projet est de développer une application cliente pour la plate-forme FlopBox(projet1) qui permette de synchroniser les données stockées à distance dans un ensemble de serveurs FTP avec le système de fichiers local d'une machine sur laquelle l'application cliente sera exécutée.

## 2-Exécution:

a)Server python:
- dans la racine on exécute la commande :
--> python3 server.py

b)Le projet1:
dans  la racine du projet1(projet1/FlobBox/) on exécute la commande :
---> mvn package 
puis la commande :
---> java -jar target/simple-service-1.0-SNAPSHOT-jar-with-dependencies.jar

on ouvre une nouvelle fenètre du terminal et on exécute la commande :
---> curl --location --request POST 'http://localhost:8080/flopbox/ftp.ubuntu.com'
---> curl --location --request PUT 'http://localhost:8080/flopbox/ftp.ubuntu.com' --header 'alias: ubuntu'

c)Le projet2:
dans  la racine du projet1(sr2/flop_box_agent/) on exécute la commande :
---> mvn package 
puis la commande :
(export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64)
---> mvn javadoc:javadoc
ensuite :
---> java -jar target/flop_box_agent-1.0-SNAPSHOT.jar

## 3-Architecture

a)-Gestion d'erreur : les erreurs sont gérées par des try, catch qui renvoie des exceptions :

exemple : 

'''

	public void downloadAll(String alias, User user,String port,String path,String myPath, boolean first) throws ConnectException {
		if( first && new File(alias+path).exists()) {
			System.out.println("delete folder -> "+myPath);
			try {
				deleteFolder(alias, user,port, myPath);
			} catch (ConnectException e) {
				e.printStackTrace();
			}
		}
		System.out.println("create folder -> "+myPath);
		try {
			createFolder(alias, user,port, myPath);
		} catch (ConnectException e) {
			throw new ConnectException(e);
		}
		String response;
		try {
			response = getList( alias,  user, port, path);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			throw new ConnectException(e);
		}
		String[] list=response.split("\n");
		for (String line:list) {
			String[] myLine=line.split(" ");	
			String name= myLine[myLine.length -1];
			if(line.startsWith("d")) {
				if(!name.startsWith(".") && !name.contains(alias)){
				downloadAll(alias, user, port,  path+"/"+name,myPath+"/"+name, false);
				}
			}
			else {
				String mypath1=myPath;
				if(myPath.startsWith("sr2/flop_box_agent")) {
						mypath1=myPath.replaceFirst("sr2/flop_box_agent/", "");
					}
					if(name!="") {
					String path2=path;
					if(path!="" ) {
						path2=path+"/";
					}
	
					download( alias,  user, port,  path2 +name,mypath1+"/"+name);
				}
			}
			
		}
		

	}
	
''' 

on vois dans cette méthode que l'éxception ConnectException est levé si on arrive pas à avoir la liste d'élements avec getList

b) énum :

--> public enum type_folder_file {file, directory
}

cet énum est utilisé afin de distingué entre un fichier et un dossier

---> public enum Download_upload_enum {
	donwload, upload
}

cet énum est utilisé afin de distingué entre un l'action donwload et l'action upload que sera utilisé dans la méthode server par l'appel de la méthode upload ou download de la class client.

## 4-Parcours du code (code samples):

a)- client.server :

'''

	public String server(String methode,String m_url, HashMap<String, String> header,Download_upload_enum download_upload,String filename) throws ConnectException {
		try{
			URL url = new URL(m_url);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(methode);
			
			if(header!= null) {
				for(Entry<String, String> mapentry : header.entrySet()) {
					connection.setRequestProperty(mapentry.getKey(),mapentry.getValue());
				}
			}
			if(download_upload==Download_upload_enum.donwload) {
			    try (InputStream in= connection.getInputStream();
			    	FileOutputStream fileOutputStream = new FileOutputStream(filename)) {			    		    
			    	int bytesRead;
			    	byte dataBuffer1[] = new byte[1024];
			    	while ((bytesRead = in.read(dataBuffer1, 0, 1024)) != -1) {
			    		  fileOutputStream.write(dataBuffer1, 0, bytesRead);
			    	}
				System.out.println("file downloaded !");
	            return "";
			    }
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			else if(download_upload==Download_upload_enum.upload) {
				connection.setDoOutput(true);
				connection.setDoInput(true);
				BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filename));
				int i;
				while ((i = bis.read()) > 0) {
					bos.write(i);
				}
				bis.close();
				bos.close();
			}
			else {
			BufferedReader read= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String response="";
			String line;
			while((line=read.readLine())!=null) {
				response+=line+"\n";
			}
			return response;
		}
		}
		catch (IOException e) {
			throw new ConnectException(e);
		}
		System.out.println("Réponse null !");
		return "";
	}

'''

cette méthode est la méthode principale de la class client elle est utilisé par les autres méthodes afin de comuniquer avec le server.
elle utilise HttpURLConnection pour se connecter au server puis puis lancer une requete graçe à l'url et la méthode passé en paramètre.
puis la réponse est traité selon la méthode qui l'a appeler.


b) client.addServer:

'''

/**
	 * Add a new server
	 * @throws ConnectException 
	 */
	public void addServer(String server) throws ConnectException {
		server("POST",s_Url+server,null,null,null);
	}
	
'''

addServer est un exemple de méthode qui utilise la méthode server.


c) client.download:

'''

	public void download(String alias, User user,String port, String filepath, String repPath) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);
	    map.put("Content-Disposition", "filename=\"" + repPath + "\"");
	    int index =filepath.lastIndexOf("/");
		String filename;
	    if(index!=-1) {
	    	filename= filepath.substring(index+1);
	    }
	    else {
	    	filename= filepath;
	    }
		System.out.println(server("GET",s_Url+alias+"/file/"+filepath, map,Download_upload_enum.donwload,repPath));
	}

'''

la méthode download utilse aussi la méthode server mais en fournissant deux paramètre qui été null dans l'exemple précident.
le premier paramètre est (Download_upload_enum.donwload) pour dire que cette action est un téléchargement et doit etre traiter différemment 
	

d) client.downloadAll :

'''

	public void downloadAll(String alias, User user,String port,String path,String myPath, boolean first) throws ConnectException {
		if( first && new File(alias+path).exists()) {
			System.out.println("delete folder -> "+myPath);
			try {
				deleteFolder(alias, user,port, myPath);
			} catch (ConnectException e) {
				e.printStackTrace();
			}
		}
		System.out.println("create folder -> "+myPath);
		try {
			createFolder(alias, user,port, myPath);
		} catch (ConnectException e) {
			throw new ConnectException(e);
		}
		String response;
		try {
			response = getList( alias,  user, port, path);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			throw new ConnectException(e);
		}
		String[] list=response.split("\n");
		for (String line:list) {
			String[] myLine=line.split(" ");	
			String name= myLine[myLine.length -1];
			if(line.startsWith("d")) {
				if(!name.startsWith(".") && !name.contains(alias)){
				downloadAll(alias, user, port,  path+"/"+name,myPath+"/"+name, false);
				}
			}
			else {
				String mypath1=myPath;
				if(myPath.startsWith("sr2/flop_box_agent")) {
						mypath1=myPath.replaceFirst("sr2/flop_box_agent/", "");
					}
					if(name!="") {
					String path2=path;
					if(path!="" ) {
						path2=path+"/";
					}
	
					download( alias,  user, port,  path2 +name,mypath1+"/"+name);
				}
			}
			
		}
		
'''

cette méthode est utilisé afin de telecharger le contenu d'un server. 
elle utilise la méthode getList pour lister les sous élement d'un dossier, parcours ces élément la et télécharge les fichier et en cas de dossier un appel récursive est fait pour traité se dossier de la même façon.

e)  myLIstofServers :

'''

	/**
	 * Update Server's List.
	 */
	public void myListOfServers() {
		try{
			URL url = new URL(s_Url);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			BufferedReader read= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String response="";
			String line;
			while((line=read.readLine())!=null) {
				response+=line;
			}
			response=response.substring(response.indexOf("[")+1, response.indexOf("]"));
			if(response.equals("")) {
				System.out.println("Liste de server vide");
			}
			else{
				Main.list_alias=new ArrayList<Server>();	
				String[] list=response.split(",");
				for (String server:list) {
					server=server.substring(server.indexOf("(")+1, server.indexOf(")")); // pour enlever '(' et ')'
					String[] serverALias=server.split(":");
					Server s= new Server(serverALias[0],serverALias[1]);
					Main.list_alias.add(s);
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		}
		
'''

cette méthode récupère la liste des server enregistrer dans le premier projet et enregistre ces dernier dans un liste de la class main de notre projet.

