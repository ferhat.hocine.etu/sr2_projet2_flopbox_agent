package com.example.rest;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * Root resource (exposed at "/" path)
 */
@Path("/")
public class Resource {

	private static HashMap<String, String> listServer = new HashMap<String, String>();
	private ResponseBuilder response;
	private String root = "/tmp/"; // The absolute path to the folder where the temporarily file is stored on the flopbox platform. 
	private int defaultPort = 21; //The default port that will be used for ports that have not been initiliazed.
	/**
	 * Load the response with an object and status code.
	 * @param status The status code the response will return.
	 * @param entity The object the response will contain.
	 */
	public void loadResponse(int status, Object entity) {
		this.response = Response.ok(entity);
		this.response.status(status);
	}

	/**
	 * Test if the server is has been registered in the Hashmap.
	 * @param server The server to test.
	 * @return True if the server has been registered, false otherwise.
	 */
	public boolean validServer(String server) {
		return (listServer.get(server) != null);
	}

	/**
	 * Initialize the port to a default one if it has not been initialized.
	 * @param port the port to initialize.
	 * @return The port initialized.
	 */
	public int initialize(int port) {
		if (port == 0) {
			return defaultPort;
		}
		return port;
	}

	@GET
	@Path("")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getAllServer() {
		String res = "[";
		for (String alias : listServer.keySet()) {
			res += "("+alias +":"+listServer.get(alias) + "),";
		}
		if(res!="[") {
		res=res.substring(0, res.length()-1);
		}
		res +="]";
		return Response.status(Response.Status.OK).entity(res+"\n").build();
	}

	/**
	 * Return the name of the server that is attached to it registered in the hashmap.
	 * @param server The name of the server to look for in the hashùa.
	 * @return The name of the server registered has value in the hashmap.
	 */
	@GET
	@Path("{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getServer(@PathParam("name") String server) {
		String result = "Server : ";
		result += this.listServer.get(server) + "\n";
		loadResponse(200, result);
		return response.build();
	}

	/**
	 * Add the name server to the hashmap.
	 * @param server The name of the server to add.
	 */
	@POST
	@Path("{name}")
	public void newServer(@PathParam("name") String server) {
		this.listServer.put(server, server);
	}

	/**
	 * Update the name of the server by giving it an alias.
	 * @param server The name of the server to give an alias.
	 * @param alias the alias name to replace the server name.
	 */
	@PUT
	@Path("{name}")
	public void updateServer(@PathParam("name") String server, @HeaderParam("alias") String alias) {
		String serverAssociated = this.listServer.remove(server);
		this.listServer.put(alias, serverAssociated);
	}

	/**
	 *  Delete a server name from the hashmap.
	 * @param server The name of the server to delete.
	 */
	@DELETE
	@Path("{name}")
	public void deleteServer(@PathParam("name") String server) {
		this.listServer.remove(server);
	}

	/**
	 * Display all the content of a directory from a FTP server.
	 * @param server The FTP server from where to get the directory informations.
	 * @param pathFile The path to the directory.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @return The informations of the content of the directory.
	 */
	@GET
	@Path("{name}/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response list(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {
			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			String result = userClient.listDirectory(pathFile);
			userClient.logout();
			userClient.disconnectClient();
			loadResponse(200, result);
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Download a file from a FTP server.
	 * @param server The server from which download the file.
	 * @param pathFile The path to the file to download.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @return The downloaded file.
	 */
	@GET
	@Path("{name}/file/{path: .*}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN })
	public Response downloadFile(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {
			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			userClient.downloadFile(pathFile);
			File file = new File(root + pathFile);
			if (file.exists()) {
				Object test = (Object) file;
				loadResponse(200, test);
				this.response.header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
			} else {
				loadResponse(404, "");
			}
			userClient.logout();
			userClient.disconnectClient();
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Upload a file to a FTP server.
	 * @param server The server from which upload the file.
	 * @param pathFile The path to the file to upload.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @param uploadInputStream The Inputstream of the file to upload.
	 * @return A message saying if the file has been uploaded.
	 */
	@POST
	@Path("{name}/file/{path: .*}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({ MediaType.TEXT_PLAIN })
	public Response uploadFile(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @FormDataParam("file") InputStream uploadInputStream) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {
			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			userClient.uploadFile(pathFile, uploadInputStream);
			userClient.logout();
			userClient.disconnectClient();
			loadResponse(200, "");
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Rename a file from a FTP server.
	 * @param server The server containing the file to rename.
	 * @param pathFile The path to the file to rename.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @param rename the name the file will be given.
	 * @return A message saying if the file has been renamed.
	 */
	@PUT
	@Path("{name}/file/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response renameFile(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @HeaderParam("rename") String rename) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {
			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			if (userClient.renameFile(pathFile, rename)) {
				loadResponse(200, "Fichier renommé.\n");
			} else {
				loadResponse(404, "Fichier non trouvé\n");
			}
			userClient.logout();
			userClient.disconnectClient();
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Rename a directory from a FTP server.
	 * @param server The server containing the directory to rename.
	 * @param pathFile The path to the directory to rename.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @param rename the name the directory will be given.
	 * @return A message saying if the directory has been renamed.
	 */
	@PUT
	@Path("{name}/directory/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response renameDirectory(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @HeaderParam("rename") String rename) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {

			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			if (userClient.renameDirectory(pathFile, rename)) {
				loadResponse(200, "Fichier renommé.\n");
			} else {
				loadResponse(404, "Fichier non trouvé\n");
			}
			userClient.logout();
			userClient.disconnectClient();

		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Delete a directory from a FTP server.
	 * @param server The server containing the directory to delete.
	 * @param pathFile The path to the directory to delete.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @return A message saying if the directory has been deleted.
	 */
	@DELETE
	@Path("{name}/directory/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteDirectory(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {

			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			if (userClient.deleteDirectory(pathFile)) {
				loadResponse(200, "Dossier supprimé.\n");
			} else {
				loadResponse(404, "Impossible de supprimer le dossier.\n");
			}
			userClient.logout();
			userClient.disconnectClient();
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

	/**
	 * Create a directory from a FTP server.
	 * @param server The server on which to create the directory.
	 * @param pathFile The path to the directory to create.
	 * @param username The username that will be used to connect to the FTP server.
	 * @param password The password that will be used to connect to the FTP server.
	 * @param port The port to which the FTP client will connect.
	 * @return A message saying if the directory has been created.
	 */
	@POST
	@Path("{name}/directory/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response createDirectory(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n");
			return this.response.build();
		}
		port = initialize(port);
		try {

			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password);
			userClient.connect();
			userClient.login();
			if (userClient.createDirectory(pathFile)) {
				loadResponse(200, "Dossier crée.\n");
			} else {
				loadResponse(404, "Impossible de crée le dossier.\n");
			}
			userClient.logout();
			userClient.disconnectClient();
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");
		}
		return this.response.build();
	}

}
