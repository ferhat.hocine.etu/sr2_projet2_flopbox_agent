package com.example.rest;

/**
 * @author Kevin Nguyen
 */

public class ConnectionServerException extends Exception {

	public ConnectionServerException(String msg) {
		super(msg);
	}
}
