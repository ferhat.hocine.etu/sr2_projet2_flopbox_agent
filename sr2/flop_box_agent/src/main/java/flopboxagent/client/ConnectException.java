package flopboxagent.client;

import java.io.IOException;

public class ConnectException extends Exception {
	public ConnectException(IOException e){
		super(e);
	}
	public ConnectException(ConnectException e){
		super(e);
	}
}
