package flopboxagent.client;
/** 
 * @author Ferhat_HOCINE
 **/

import java.awt.List;

/** 
 * @author Ferhat_HOCINE
 **/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

import flopboxagent.Main;
import flopboxagent.User.User;
import flopboxagent.server.Server;

/**
 * client class
 */
public class client {
	private HttpClient client;
	private String s_Url="http://localhost:8080/flopbox/";

/**
 * Constructor
 */
	public client() {
		
	}
	
	/**
	 * Update Server's List.
	 */
	public void myListOfServers() {
		try{
			URL url = new URL(s_Url);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			BufferedReader read= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String response="";
			String line;
			while((line=read.readLine())!=null) {
				response+=line;
			}
			response=response.substring(response.indexOf("[")+1, response.indexOf("]"));
			if(response.equals("")) {
				System.out.println("Liste de server vide");
			}
			else{
				Main.list_alias=new ArrayList<Server>();	
				String[] list=response.split(",");
				for (String server:list) {
					server=server.substring(server.indexOf("(")+1, server.indexOf(")")); // pour enlever '(' et ')'
					String[] serverALias=server.split(":");
					Server s= new Server(serverALias[0],serverALias[1]);
					Main.list_alias.add(s);
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		}
	
	/**
	 * Add a new server
	 * @throws ConnectException 
	 */
	public void addServer(String server) throws ConnectException {
		server("POST",s_Url+server,null,null,null);
	}
	
	/**
	 * Add a new alias to existing sever
	 * @throws ConnectException 
	 */
	public void addServerAlias(String server, String alias) throws ConnectException {
	     HashMap<String,String> map = new HashMap<String,String>();
	     map.put("alias" ,alias);
	     server("PUT",s_Url+server, map,null,null);
	}
	
	/**
	 * Add a new alias to existing sever
	 * @param String alias
	 * @return String server url  that correspend to alias in parametre
	 * @throws ConnectException 
	 */
	public String getServerUrl(String alias) throws ConnectException {
		String result =server("GET",s_Url+alias, null,null,null);
		System.out.println(result);
		return result;
	}
	
	/**
	 * display the list of files and folders in the current directory
	 * @param String alias
	 * @param User user
	 * @param port
	 * @throws ConnectException 
	 */
	public String getList(String alias, User user,String port,String path) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);
		return server("GET",s_Url+alias+path+"/", map,null,null);
	}
	
	/**
	 * download file
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String filepath
	 * @throws ConnectException 
	 */
	public void download(String alias, User user,String port, String filepath, String repPath) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);
	    map.put("Content-Disposition", "filename=\"" + repPath + "\"");
	    int index =filepath.lastIndexOf("/");
		String filename;
	    if(index!=-1) {
	    	filename= filepath.substring(index+1);
	    }
	    else {
	    	filename= filepath;
	    }
		System.out.println(server("GET",s_Url+alias+"/file/"+filepath, map,Download_upload_enum.donwload,repPath));
	}
	

	/**
	 * rename a file or folder
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @param String newfilename
	 * @param type_folder_file t
	 * @throws ConnectException 
	 */
	public void rename(String alias, User user,String port,String path, String newFilename,type_folder_file t) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);
	    map.put("rename",newFilename);

		System.out.println(server("PUT",s_Url+alias+"/"+t+"/"+path, map,null,null));
	}
	

	/**
	 * create folder
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @throws ConnectException 
	 */
	public void createFolder(String alias, User user,String port,String path) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);

		System.out.println(server("POST",s_Url+alias+"/directory/"+path, map,null,null));
	}
	

	/**
	 * rename a file or folder
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @throws ConnectException 
	 */
	public void deleteFolder(String alias, User user,String port,String path) throws ConnectException {
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);

		System.out.println(server("DELETE",s_Url+alias+"/directory/"+path, map,null,null));
	}
	
	

	/**
	 * upload a file or folder
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @throws ConnectException 
	 */
	public void upload(String alias, User user,String port, String filepath) throws ConnectException {
		String filename;
	    int index =filepath.lastIndexOf("/");
	    if(index!=-1) {
	    	filename= filepath.substring(index+1);
	    }
	    else {
	    	filename= filepath;
	    }
	    HashMap<String,String> map = new HashMap<String,String>();
	    map.put("username",user.getIdent());
	    map.put("password",user.getPassword());
	    map.put("port",port);
	    map.put("Content-Disposition", "file=\"a3.txt\"");

		System.out.println(server("POST",s_Url+alias+"/file/"+filepath, map,Download_upload_enum.upload,"a3.txt"));
	}
	
	

	/**
	 * server is the methode used to connect and get information
	 * @param String methode
	 * @param String m_url
	 * @param HashMap<String, String>  header
	 * @param Download_upload_enum download_upload
	 * @param String filename
	 * @return String
	 * @throws ConnectException 
	 */
	public String server(String methode,String m_url, HashMap<String, String> header,Download_upload_enum download_upload,String filename) throws ConnectException {
		try{
			URL url = new URL(m_url);
			HttpURLConnection connection= (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(methode);
			
			if(header!= null) {
				for(Entry<String, String> mapentry : header.entrySet()) {
					connection.setRequestProperty(mapentry.getKey(),mapentry.getValue());
				}
			}
			if(download_upload==Download_upload_enum.donwload) {
			    try (InputStream in= connection.getInputStream();
			    	FileOutputStream fileOutputStream = new FileOutputStream(filename)) {			    		    
			    	int bytesRead;
			    	byte dataBuffer1[] = new byte[1024];
			    	while ((bytesRead = in.read(dataBuffer1, 0, 1024)) != -1) {
			    		  fileOutputStream.write(dataBuffer1, 0, bytesRead);
			    	}
				System.out.println("file downloaded !");
	            return "";
			    }
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			else if(download_upload==Download_upload_enum.upload) {
				connection.setDoOutput(true);
				connection.setDoInput(true);
				BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filename));
				int i;
				while ((i = bis.read()) > 0) {
					bos.write(i);
				}
				bis.close();
				bos.close();
			}
			else {
			BufferedReader read= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String response="";
			String line;
			while((line=read.readLine())!=null) {
				response+=line+"\n";
			}
			return response;
		}
		}
		catch (IOException e) {
			throw new ConnectException(e);
		}
		System.out.println("Réponse null !");
		return "";
	}
	
	
	/**
	 * downloadAll is the methode used to download all from server
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @param String myPath
	 * @param boolean first
	 * @throws ConnectException 
	 */
	public void downloadAll(String alias, User user,String port,String path,String myPath, boolean first) throws ConnectException {
		if( first && new File(alias+path).exists()) {
			System.out.println("delete folder -> "+myPath);
			try {
				deleteFolder(alias, user,port, myPath);
			} catch (ConnectException e) {
				e.printStackTrace();
			}
		}
		System.out.println("create folder -> "+myPath);
		try {
			createFolder(alias, user,port, myPath);
		} catch (ConnectException e) {
			throw new ConnectException(e);
		}
		String response;
		try {
			response = getList( alias,  user, port, path);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			throw new ConnectException(e);
		}
		String[] list=response.split("\n");
		for (String line:list) {
			String[] myLine=line.split(" ");	
			String name= myLine[myLine.length -1];
			if(line.startsWith("d")) {
				if(!name.startsWith(".") && !name.contains(alias)){
				downloadAll(alias, user, port,  path+"/"+name,myPath+"/"+name, false);
				}
			}
			else {
				String mypath1=myPath;
				if(myPath.startsWith("sr2/flop_box_agent")) {
						mypath1=myPath.replaceFirst("sr2/flop_box_agent/", "");
					}
					if(name!="") {
					String path2=path;
					if(path!="" ) {
						path2=path+"/";
					}
	
					download( alias,  user, port,  path2 +name,mypath1+"/"+name);
				}
			}
			
		}
		

	}
	
	/**
	 * listOfDirectory is the methode used get all subFolder
	 * @param String alias
	 * @param User user
	 * @param String port
	 * @param String path
	 * @return ArrayList<String> 
	 * @throws ConnectException 
	 */
	public ArrayList<String> listOfDirectory(String alias, User user,String port,String path) throws ConnectException {
		String response;
		ArrayList<String> res = new ArrayList<String>();

		try {
			response = getList( alias,  user, port, path);
		} catch (ConnectException e) {
			throw new ConnectException(e);
		}
		String[] list=response.split("\n");
		for (String line:list) {
			String[] myLine=line.split(" ");	
			String name= myLine[myLine.length -1];
			if(line.startsWith("d")) {
				if(!name.startsWith(".") && !name.contains(alias)){		
				res.add(path+"/"+name);
				ArrayList<String> res2=listOfDirectory(alias, user,port, path+"/"+name);
				res.addAll(res2);
				}
			}
			}
		return res;
		
	}
	
	

}
