package flopboxagent;
/** 
 * @author Ferhat_HOCINE
 **/

import java.awt.List;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat.Type;

import flopboxagent.User.User;
import flopboxagent.client.ConnectException;
import flopboxagent.client.client;
import flopboxagent.client.type_folder_file;
import flopboxagent.server.Server;
import watchService.WatchDir;

/**
 * MAIN class
 */
public class Main {
	public static ArrayList<Server> list_alias = new ArrayList<Server>();	    
	  public static void main( String[] args ) throws ConnectException, InterruptedException, IOException
	    {

		 client c= new client(); //création de client 
		 c.addServer("localhost"); //ajout du server localhost
		 c.addServerAlias("localhost", "lc"); // afectation d'un l'Alias
		 User user= new User("hocine","ferhat"); //création d'un user
		 c.myListOfServers(); //récupération de la liste de server
		 for(Server s:list_alias) {
			 System.out.println("server : "+s.getServer() +" - Alias : "+s.getAlis());
		 }
		 String port = "2121"; // création d'un port
		 System.out.println(c.getList("lc",user,port,"")); //affichage des élement du server
		 c.downloadAll("lc",user,port,"","sr2/flop_box_agent/lc", true); //télechargement de tous les élément du server localhost

		 WatchDir w = new WatchDir(Paths.get("lc"), true,c,"lc",user,port); // création d'un instance de la classe WatchDir qui surveille les changement dans le local
		 w.processEvents(); // éxécution de la classe
	    }
}
