package flopboxagent.User;
/** 
 * @author Ferhat_HOCINE
 **/


/**
 * Class to represent a user 
 */
public class User {
	private String ident;
	private String password;
	/**
	 * Constructor
	 */
	public User(String ident, String password) {
		this.ident = ident;
		this.password = password;
	}
	
    /**
     * getter
     */
	public String getIdent() {
		return this.ident;
	}
    /**
     * getter
     */
	public String getPassword() {
		return this.password;
	}
    /**
     * setter
     */
	public void setIdent(String ident) {
		this.ident = ident;
	}
    /**
     * setter
     */
	public void setPassword(String pass) {
		this.password = pass;
	}
    /**
     * equal methode to compare an instance of User to the current instance
     */
	public boolean equals(User user) {
		if (user.getIdent().equals(this.ident) && user.getPassword().equals(this.password)) {
			return true;
		}
		return false;
	}
}