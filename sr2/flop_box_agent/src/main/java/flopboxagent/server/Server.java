package flopboxagent.server;
/** 
 * @author Ferhat_HOCINE
 **/

/**
 * Class to represent server with an alias to call it 
 */
public class Server {
	private String alis;
	private String server;
    /**
     * Constructor
     */
	public Server(String alis, String server) {
		this.alis = alis;
		this.server = server;
	}
	
    /**
     * getter
     */
	public String getAlis() {
		return this.alis;
	}
    /**
     * getter
     */
	public String getServer() {
		return this.server;
	}
    /**
     * setter
     */
	public void setAlis(String alis1) {
		this.alis = alis1;
	}
    /**
     * setter
     */
	public void setServer(String server1) {
		this.server = server;
	}
    /**
     * equal methode to compare an instance of alias to the current instance
     */
	public boolean equals(Server al) {
		if (al.getAlis().equals(this.alis) && al.getServer().equals(this.server)) {
			return true;
		}
		return false;
	}
}